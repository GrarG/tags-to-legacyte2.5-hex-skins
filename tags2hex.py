import PySimpleGUI as sg
import ast
import json
import pyperclip as ppy
from paths import Paths as p

sg.theme('LightGrey2')
sg.set_options(use_ttk_buttons=False,
            margins=(0,0),
            element_padding=(0, 0),
            border_width=0,
            button_color=(sg.theme_background_color(), sg.theme_background_color()))


buttonspath = "./graphics/buttons/"
panel1path = buttonspath + "panel1/"
panel2path = buttonspath + "panel2/"
panel5path = buttonspath + "panel5/"

TEmap = None

with open("charactertable.json", "r") as f:
    TEmap = json.load(f)

def transform(charin=None):
    if charin in TEmap:
        return TEmap[charin]
    else:
        return ""

panel11 = [[sg.Button("", image_filename=panel1path+p.fetch(" "), auto_size_button=False, size=(54,32), key=" "), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("@"), key="@")],
            [sg.Button("", image_filename=panel1path+p.fetch("("), size=(54,32), key="("), sg.Button("", image_filename=panel1path+p.fetch(")"), size=(54,32), key=")")],
            [sg.Button("", image_filename=panel1path+p.fetch("^"), size=(32,32), key="^"), sg.Button("", image_filename=panel1path+p.fetch(":"), size=(32,32), key=":"), sg.Button("", image_filename=panel1path+p.fetch(";"), size=(32,32), key=";")]]

panel12 = [[sg.Button("", image_filename=panel1path+p.fetch("A"), auto_size_button=False, size=(54,32), key="A"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("a"), key="a")],
            [sg.Button("", image_filename=panel1path+p.fetch("B"), size=(54,32), key="B"), sg.Button("", image_filename=panel1path+p.fetch("b"), size=(54,32), key="b")],
            [sg.Button("", image_filename=panel1path+p.fetch("C"), size=(54,32), key="C"), sg.Button("", image_filename=panel1path+p.fetch("c"), size=(54,32), key="c")]]

panel13 = [[sg.Button("", image_filename=panel1path+p.fetch("D"), auto_size_button=False, size=(54,32), key="D"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("d"), key="d")],
            [sg.Button("", image_filename=panel1path+p.fetch("E"), size=(54,32), key="E"), sg.Button("", image_filename=panel1path+p.fetch("e"), size=(54,32), key="e")],
            [sg.Button("", image_filename=panel1path+p.fetch("F"), size=(54,32), key="F"), sg.Button("", image_filename=panel1path+p.fetch("f"), size=(54,32), key="f")]]

panel14 = [[sg.Button("", image_filename=panel1path+p.fetch("G"), auto_size_button=False, size=(54,32), key="G"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("g"), key="g")],
            [sg.Button("", image_filename=panel1path+p.fetch("H"), size=(54,32), key="H"), sg.Button("", image_filename=panel1path+p.fetch("h"), size=(54,32), key="h")],
            [sg.Button("", image_filename=panel1path+p.fetch("I"), size=(54,32), key="I"), sg.Button("", image_filename=panel1path+p.fetch("i"), size=(54,32), key="i")]]

panel15 = [[sg.Button("", image_filename=panel1path+p.fetch("J"), auto_size_button=False, size=(54,32), key="J"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("j"), key="j")],
            [sg.Button("", image_filename=panel1path+p.fetch("K"), size=(54,32), key="K"), sg.Button("", image_filename=panel1path+p.fetch("k"), size=(54,32), key="k")],
            [sg.Button("", image_filename=panel1path+p.fetch("L"), size=(54,32), key="L"), sg.Button("", image_filename=panel1path+p.fetch("l"), size=(54,32), key="l")]]

panel16 = [[sg.Button("", image_filename=panel1path+p.fetch("M"), auto_size_button=False, size=(54,32), key="M"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("m"), key="m")],
            [sg.Button("", image_filename=panel1path+p.fetch("N"), size=(54,32), key="N"), sg.Button("", image_filename=panel1path+p.fetch("n"), size=(54,32), key="n")],
            [sg.Button("", image_filename=panel1path+p.fetch("O"), size=(54,32), key="O"), sg.Button("", image_filename=panel1path+p.fetch("o"), size=(54,32), key="o")]]

panel17 = [[sg.Button("", image_filename=panel1path+p.fetch("P"), auto_size_button=False, size=(32,32), key="P"), sg.Button("", size=(32,32), image_filename=panel1path+p.fetch("Q"), key="Q"), sg.Button("", size=(32,32), image_filename=panel1path+p.fetch("R"), key="R")],
            [sg.Button("", image_filename=panel1path+p.fetch("S"), size=(32,32), key="S"), sg.Button("", image_filename=panel1path+p.fetch("p"), size=(32,32), key="p"), sg.Button("", size=(32,32), image_filename=panel1path+p.fetch("q"), key="q")],
            [sg.Button("", image_filename=panel1path+p.fetch("r"), size=(32,32), key="r"), sg.Button("", image_filename=panel1path+p.fetch("s"), size=(32,32), key="s")]]

panel18 = [[sg.Button("", image_filename=panel1path+p.fetch("T"), auto_size_button=False, size=(54,32), key="T"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("t"), key="t")],
            [sg.Button("", image_filename=panel1path+p.fetch("U"), size=(54,32), key="U"), sg.Button("", image_filename=panel1path+p.fetch("u"), size=(54,32), key="u")],
            [sg.Button("", image_filename=panel1path+p.fetch("V"), size=(54,32), key="V"), sg.Button("", image_filename=panel1path+p.fetch("v"), size=(54,32), key="v")]]

panel19 = [[sg.Button("", image_filename=panel1path+p.fetch("W"), auto_size_button=False, size=(32,32), key="W"), sg.Button("", size=(32,32), image_filename=panel1path+p.fetch("X"), key="X"), sg.Button("", size=(32,32), image_filename=panel1path+p.fetch("Y"), key="Y")],
            [sg.Button("", image_filename=panel1path+p.fetch("Z"), size=(32,32), key="Z"), sg.Button("", image_filename=panel1path+p.fetch("w"), size=(32,32), key="w"), sg.Button("", size=(32,32), image_filename=panel1path+p.fetch("x"), key="x")],
            [sg.Button("", image_filename=panel1path+p.fetch("y"), size=(54,32), key="y"), sg.Button("", image_filename=panel1path+p.fetch("z"), size=(54,32), key="z")]]

panel110 = [[sg.Button("", image_filename=panel1path+p.fetch("?"), auto_size_button=False, size=(54,32), key="?"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch("!"), key="!")],
            [sg.Button("", image_filename=panel1path+p.fetch("&"), size=(54,32), key="&"), sg.Button("", image_filename=panel1path+p.fetch("%"), size=(54,32), key="%")],
            [sg.Button("", disabled=True, image_filename=buttonspath+p.fetch("emptybig"), size=(54,32))]]

panel111 = [[sg.Button("", image_filename=panel1path+p.fetch("·"), auto_size_button=False, size=(54,32), key="·"), sg.Button("", size=(54,32), image_filename=panel1path+p.fetch(","), key=",")],
            [sg.Button("", image_filename=panel1path+p.fetch("."), size=(54,32), key="."), sg.Button("", image_filename=panel1path+p.fetch("/"), size=(54,32), key="/")],
            [sg.Button("", image_filename=panel1path+p.fetch("~"), size=(54,32), key="~")]]

panel1 = [[sg.Column(panel11), sg.T("", pad=(1,0)), sg.Column(panel12), sg.T("", pad=(1,0)), sg.Column(panel13)],
            [sg.Canvas(size=(0,4))],
            [sg.Column(panel14), sg.T("", pad=(1,0)), sg.Column(panel15), sg.T("", pad=(1,0)), sg.Column(panel16)],
            [sg.Canvas(size=(0,4))],
            [sg.Column(panel17), sg.T("", pad=(1,0)), sg.Column(panel18), sg.T("", pad=(1,0)), sg.Column(panel19)],
            [sg.Canvas(size=(0,4))],
            [sg.Column(panel110), sg.T("", pad=(1,0)), sg.Column(panel111)]]

panel21 = [[sg.Button("", image_filename=panel2path+p.fetch("Á"), auto_size_button=False, size=(32,32), key="Á"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("À"), key="À"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Â"), key="Â")],
            [sg.Button("", image_filename=panel2path+p.fetch("Ä"), size=(32,32), key="Ä"), sg.Button("", image_filename=panel2path+p.fetch("á"), size=(32,32), key="á"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("à"), key="à")],
            [sg.Button("", image_filename=panel2path+p.fetch("â"), size=(32,32), key="â"), sg.Button("", image_filename=panel2path+p.fetch("ä"), size=(32,32), key="ä")]]

panel22 = [[sg.Button("", image_filename=panel2path+p.fetch("Ç"), auto_size_button=False, size=(54,32), key="Ç"), sg.Button("", size=(54,32), image_filename=panel2path+p.fetch("ç"), key="ç")],
            [sg.Button("", image_filename=panel2path+p.fetch("Œ"), auto_size_button=False, size=(54,32), key="Œ"), sg.Button("", size=(54,32), image_filename=panel2path+p.fetch("œ"), key="œ")]]

panel23 = [[sg.Button("", image_filename=panel2path+p.fetch("É"), auto_size_button=False, size=(32,32), key="É"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("È"), key="È"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Ê"), key="Ê")],
            [sg.Button("", image_filename=panel2path+p.fetch("Ë"), size=(32,32), key="Ë"), sg.Button("", image_filename=panel2path+p.fetch("é"), size=(32,32), key="é"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("è"), key="è")],
            [sg.Button("", image_filename=panel2path+p.fetch("ê"), size=(32,32), key="ê"), sg.Button("", image_filename=panel2path+p.fetch("ë"), size=(32,32), key="ë")]]

panel24 = [[sg.Button("", image_filename=panel2path+p.fetch("Í"), auto_size_button=False, size=(32,32), key="Í"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Ì"), key="Ì"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Î"), key="Î")],
            [sg.Button("", image_filename=panel2path+p.fetch("Ï"), size=(32,32), key="Ï"), sg.Button("", image_filename=panel2path+p.fetch("í"), size=(32,32), key="í"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("ì"), key="ì")],
            [sg.Button("", image_filename=panel2path+p.fetch("î"), size=(32,32), key="î"), sg.Button("", image_filename=panel2path+p.fetch("ï"), size=(32,32), key="ï")]]

panel25 = [[sg.Button("", image_filename=panel2path+p.fetch("Ó"), auto_size_button=False, size=(32,32), key="Ó"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Ò"), key="Ò"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Ô"), key="Ô")],
            [sg.Button("", image_filename=panel2path+p.fetch("Ö"), size=(32,32), key="Ö"), sg.Button("", image_filename=panel2path+p.fetch("ó"), size=(32,32), key="ó"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("ò"), key="ò")],
            [sg.Button("", image_filename=panel2path+p.fetch("ô"), size=(32,32), key="ô"), sg.Button("", image_filename=panel2path+p.fetch("ö"), size=(32,32), key="ö")]]

panel26 = [[sg.Button("", image_filename=panel2path+p.fetch("Ñ"), auto_size_button=False, size=(54,32), key="Ñ"), sg.Button("", size=(54,32), image_filename=panel2path+p.fetch("ñ"), key="ñ")],
            [sg.Button("", image_filename=panel2path+p.fetch("Ϸ"), size=(54,32), key="Ϸ"), sg.Button("", image_filename=panel2path+p.fetch("ϸ"), size=(54,32), key="ϸ")],
            [sg.Button("", image_filename=panel2path+p.fetch("ß"), size=(54,32), key="ß")]]

panel27 = [[sg.Button("", image_filename=panel2path+p.fetch("Ú"), auto_size_button=False, size=(32,32), key="Ú"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Ù"), key="Ù"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("Û"), key="Û")],
            [sg.Button("", image_filename=panel2path+p.fetch("Ü"), size=(32,32), key="Ü"), sg.Button("", image_filename=panel2path+p.fetch("ú"), size=(32,32), key="ú"), sg.Button("", size=(32,32), image_filename=panel2path+p.fetch("ù"), key="ù")],
            [sg.Button("", image_filename=panel2path+p.fetch("û"), size=(32,32), key="û"), sg.Button("", image_filename=panel2path+p.fetch("ü"), size=(32,32), key="ü")]]

panel28 = [[sg.Button("", image_filename=panel2path+p.fetch("¡"), auto_size_button=False, size=(54,32), key="¡"), sg.Button("", size=(54,32), image_filename=panel2path+p.fetch("¿"), key="¿")],
            [sg.Button("", image_filename=panel2path+p.fetch("£"), size=(54,32), key="£"), sg.Button("", image_filename=panel2path+p.fetch("€"), size=(54,32), key="€")]]

panel29 = [[sg.Button("", image_filename=panel2path+p.fetch("_"), auto_size_button=False, size=(54,32), key="_"), sg.Button("", size=(54,32), image_filename=panel2path+p.fetch("´"), key="´")],
            [sg.Button("", image_filename=panel2path+p.fetch("º"), size=(54,32), key="º")]]

panel211 = [[sg.Button("", image_filename=panel2path+p.fetch("↑"), auto_size_button=False, size=(54,32), key="↑"), sg.Button("", size=(54,32), image_filename=panel2path+p.fetch("↓"), key="↓")],
            [sg.Button("", image_filename=panel2path+p.fetch("→"), size=(54,32), key="→"), sg.Button("", image_filename=panel2path+p.fetch("←"), size=(54,32), key="←")]]

panel2 = [[sg.Column(panel21), sg.T("", pad=(1,0)), sg.Column(panel22), sg.T("", pad=(1,0)), sg.Column(panel23)],
            [sg.Canvas(size=(0,4))],
            [sg.Column(panel24), sg.T("", pad=(1,0)), sg.Column(panel25), sg.T("", pad=(1,0)), sg.Column(panel26)],
            [sg.Canvas(size=(0,4))],
            [sg.Column(panel27), sg.T("", pad=(1,0)), sg.Column(panel28), sg.T("", pad=(1,0)), sg.Column(panel29)],
            [sg.Canvas(size=(0,4))],
            [sg.Canvas(size=(108,0)), sg.T("", pad=(1,0)), sg.Column(panel211), sg.T("", pad=(1,0)), sg.Button("", image_filename=buttonspath+"delete.png", auto_size_button=False, key="Delete", size=(108,72))],
            ]

panel510 = [[sg.Button("", image_filename=panel5path+p.fetch("-"), auto_size_button=False, size=(54,32), key="-"), sg.Button("", size=(54,32), image_filename=panel5path+p.fetch("+"), key="+")],
            [sg.Button("", image_filename=panel5path+p.fetch("×"), size=(54,32), key="×"), sg.Button("", image_filename=panel5path+p.fetch("="), size=(54,32), key="=")],
            [sg.Button("", disabled=True, image_filename=buttonspath+p.fetch("emptybig"), size=(54,32))]]

panel5 = [[sg.Button("", image_filename=panel5path+p.fetch("1"), auto_size_button=False, size=(108,108), key="1"), sg.T("", pad=(1,0)), sg.Button("", image_filename=panel5path+p.fetch("2"), auto_size_button=False, size=(108,108), key="2"), sg.T("", pad=(1,0)), sg.Button("", image_filename=panel5path+p.fetch("3"), auto_size_button=False, size=(108,108), key="3")],
            [sg.Canvas(size=(0,4))],
            [sg.Button("", image_filename=panel5path+p.fetch("4"), auto_size_button=False, size=(108,108), key="4"), sg.T("", pad=(1,0)), sg.Button("", image_filename=panel5path+p.fetch("5"), auto_size_button=False, size=(108,108), key="5"), sg.T("", pad=(1,0)), sg.Button("", image_filename=panel5path+p.fetch("6"), auto_size_button=False, size=(108,108), key="6")],
            [sg.Canvas(size=(0,4))],
            [sg.Button("", image_filename=panel5path+p.fetch("7"), auto_size_button=False, size=(108,108), key="7"), sg.T("", pad=(1,0)), sg.Button("", image_filename=panel5path+p.fetch("8"), auto_size_button=False, size=(108,108), key="8"), sg.T("", pad=(1,0)), sg.Button("", image_filename=panel5path+p.fetch("9"), auto_size_button=False, size=(108,108), key="9")],
            [sg.Canvas(size=(0,4))],
            [sg.Column(panel510), sg.T("", pad=(1,0)), sg.Button("", image_filename=panel5path+p.fetch("0"), auto_size_button=False, size=(108,108), key='0')]
            ]

layout = [  [sg.Canvas(size=(0,20))],
            [sg.T("", size=(2,0)), sg.Column(panel1), sg.T("", size=(1,0)), sg.Column(panel2), sg.T("", size=(1,0)), sg.Column(panel5), sg.T("", size=(2,0))],
            [sg.Canvas(size=(0,40))],
            [sg.Column([[sg.Text("Tag:")]], justification='center')],
            [sg.Column([[sg.InputText(key="tag", justification='center', font=("Consolas", 34), size=(22,1))]], justification='center')],
            [sg.Column([[sg.Text("Hex:", justification='center')]], justification="center")],
            [sg.Column([[sg.InputText(disabled=True, key="hex", justification='center', font=('Consolas', 34), size=(22,1))]], justification="center")],
            [sg.Canvas(size=(0,25))],
            [sg.Column([[sg.Button("", image_filename=buttonspath+"copy.png", auto_size_button=False, size=(216,80), key="Copy")]], justification="center")],
            [sg.Canvas(size=(0,40))]]

window = sg.Window('Tag To LTE2.5 Hex', layout)

while True:
    event, values = window.read()
    if event in (None, 'Exit'):
        break
    elif event == "Copy":
        ppy.copy(values["hex"])
    elif event == "Delete":
        tag = values["tag"]
        updt = ""
        if len(tag) > 0:
            tag = tag[:-1]
            for char in tag:
                updt += transform(charin=char)
            window.Element("tag").Update(tag)
            window.Element("hex").Update(updt)
        
    else:
        ## This check should not be here but I'm fairly certain
        ## this is a bug in PySimpleGUI
        if len(event) > 1:
            event = event[0]
        tag = values["tag"]
        if len(tag) > 4:
            tag = tag[:4]

        tag += event
        updt = ""
        for char in tag:
            updt += transform(charin=char)
        window.Element("tag").Update(tag)
        window.Element("hex").Update(updt)

window.close()
