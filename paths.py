class Paths:
    paths = {
        " ":"1.1space.png", "@":"1.2at.png",
        "(":"1.3startparenth.png", ")":"1.4endparenth.png",
        "^":"1.5up.png", ":":"1.6twodots.png",
        ";":"1.7dotcomma.png",

        "A":"2.1A.png", "a":"2.2a.png",
        "B":"2.3B.png", "b":"2.4b.png",
        "C":"2.5C.png", "c":"2.6c.png",
        "D":"3.1D.png", "d":"3.2d.png",
        "E":"3.3E.png", "e":"3.4e.png",
        "F":"3.5F.png", "f":"3.6f.png",

        "G":"4.1G.png", "g":"4.2g.png",
        "H":"4.3H.png", "h":"4.4h.png",
        "I":"4.5I.png", "i":"4.6i.png",

        "J":"5.1J.png", "j":"5.2j.png",
        "K":"5.3K.png", "k":"5.4k.png",
        "L":"5.5L.png", "l":"5.6l.png",

        "M":"6.1M.png", "m":"6.2m.png",
        "N":"6.3N.png", "n":"6.4n.png",
        "O":"6.5O.png", "o":"6.6o.png",

        "P":"7.1P.png", "p":"7.2p.png",
        "Q":"7.3Q.png", "q":"7.4q.png",
        "R":"7.5R.png", "r":"7.6r.png",
        "S":"7.7S.png", "s":"7.8s.png",

        "T":"8.1T.png", "t":"8.2t.png",
        "U":"8.3U.png", "u":"8.4u.png",
        "V":"8.5V.png", "v":"8.6v.png",

        "W":"9.1W.png", "w":"9.2w.png",
        "X":"9.3X.png", "x":"9.4x.png",
        "Y":"9.5Y.png", "y":"9.6y.png",
        "Z":"9.7Z.png", "z":"9.8z.png",

        "?":"10.1interr.png", "!":"10.2excl.png",
        "&":"10.3and.png", "%":"10.4perc.png",

        "·":"11.1highdot.png", ",":"11.2comma.png",
        ".":"11.3dot.png", "/":"11.4dash.png",
        "~":"11.5tilde.png",

        "1":"1.1one.png", "2":"2.1two.png",
        "3":"3.1three.png", "4":"4.1four.png",
        "5":"5.1five.png", "6":"6.1six.png",
        "7":"7.1seven.png", "8":"8.1eight.png",
        "9":"9.1nine.png", "0":"11.1zero.png",

        "-":"10.1minus.png", "+":"10.2plus.png",
        "×":"10.3mult.png", "=":"10.4equals.png",

        "Á":"1.1aright.png", "À":"1.2aleft.png",        
        "Â":"1.3acflex.png", "Ä":"1.4aumlaut.png",
        "á":"1.5aright.png", "à":"1.6aleft.png",
        "â":"1.7acflex.png", "ä":"1.8aumlaut.png",

        "Ç":"2.1Cedilla.png", "ç":"2.2cedilla.png",
        "Œ":"2.3oe.png", "œ":"2.4oe.png",

        "É":"3.1eright.png", "È":"3.2eleft.png",        
        "Ê":"3.3ecflex.png", "Ë":"3.4eumlaut.png",
        "é":"3.5eright.png", "è":"3.6eleft.png",
        "ê":"3.7ecflex.png", "ë":"3.8eumlaut.png",

        "Í":"4.1iright.png", "Ì":"4.2ileft.png",        
        "Î":"4.3icflex.png", "Ï":"4.4iumlaut.png",
        "í":"4.5iright.png", "ì":"4.6ileft.png",
        "î":"4.7icflex.png", "ï":"4.8iumlaut.png",

        "Ó":"5.1oright.png", "Ò":"5.2oleft.png",        
        "Ô":"5.3ocflex.png", "Ö":"5.4oumlaut.png",
        "ó":"5.5oright.png", "ò":"5.6oleft.png",
        "ô":"5.7ocflex.png", "ö":"5.8oumlaut.png",

        "Ú":"7.1uright.png", "Ù":"7.2uleft.png",        
        "Û":"7.3ucflex.png", "Ü":"7.4uumlaut.png",
        "ú":"7.5uright.png", "ù":"7.6uleft.png",
        "û":"7.7ucflex.png", "ü":"7.8uumlaut.png",

        "¡":"8.1exclesp.png", "¿":"8.2interresp.png",
        "£":"8.3pound.png", "€":"8.4eur.png",

        "Ñ":"6.1ene.png", "ñ":"6.2ene.png",
        "Ϸ":"6.3sho.png", "ϸ":"6.4sho.png",
        "ß":"6.5eszett.png",

        "_":"9.1lowdash.png", "´":"9.2accent.png",
        "º":"9.3degrees.png",

        "↑":"11.1up.png", "↓":"11.2down.png",
        "→":"11.3right.png", "←":"11.4left.png",

        "emptybig":"empty.png", "emptysmall":"emptysmall.png"
    }

    def fetch(key):
        return Paths.paths[key]
